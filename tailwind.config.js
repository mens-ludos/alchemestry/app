const animate = require('tailwindcss-animate');

/** @type {import('tailwindcss').Config} */
module.exports = {
  darkMode: ['class'],
  safelist: ['dark'],
  prefix: '',
  content: [
    './pages/**/*.{ts,tsx,vue}',
    './components/**/*.{ts,tsx,vue}',
    './app/**/*.{ts,tsx,vue}',
    './src/**/*.{ts,tsx,vue}',
  ],
  theme: {
    container: {
      center: true,
      padding: '2rem',
      screens: {
        '2xl': '1400px',
      },
    },
    extend: {
      colors: {
        background: {
          DEFAULT: '#0b1826',
        },
        foreground: {
          DEFAULT: '#f4e6b0',
        },
        sidebar: {
          DEFAULT: '#0b1420',
        },
      },
      fontSize: {
        16: '4rem',
      },
      width: {
        15: '3.75rem',
      },
      height: {
        18.5: '4.625rem',
      },
      maxWidth: {
        'sidebar-lg': '30.188rem',
        'sidebar-md': '6.875rem',
      },
      fontFamily: {
        albert: ['Albert Text', 'sans-serif'],
        montserrat: ['Montserrat', 'sans-serif'],
        arimo: ['Arimo', 'sans-serif'],
      },
    },
  },
  plugins: [animate],
};
