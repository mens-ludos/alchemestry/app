import '@/assets/main.css';

import { VueQueryPlugin } from '@tanstack/vue-query';
import { MotionPlugin } from '@vueuse/motion';
import { createPinia } from 'pinia';
import { UseWagmiPlugin } from 'use-wagmi';
import { createApp } from 'vue';

import App from '@/App.vue';
import { wagmiConfig } from '@/lib/wagmi';
import { initWeb3Modal } from '@/lib/web3modal';
import { router } from '@/router';

const app = createApp(App);

const pinia = createPinia();
app.use(pinia);

app.use(router);

app.use(MotionPlugin);

app.use(UseWagmiPlugin, { config: wagmiConfig, reconnectOnMount: true });
app.use(VueQueryPlugin);

initWeb3Modal();

app.mount('#app');
