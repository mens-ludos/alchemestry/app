import { useAccount } from 'use-wagmi';
import { computed } from 'vue';

export const useReferralLink = () => {
  const { address } = useAccount();
  return computed(
    () => `${window.location.origin}/?ref=${encodeURIComponent(window.btoa(address.value ?? ''))}`,
  );
};
