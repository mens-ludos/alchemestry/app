import { h, type VNodeTypes } from 'vue';

import DiscordIcon from '@/components/icons/DiscordIcon.vue';
import FaqIcon from '@/components/icons/FaqIcon.vue';
import StoreIcon from '@/components/icons/StoreIcon.vue';
import TelegramIcon from '@/components/icons/TelegramIcon.vue';
import TwitterIcon from '@/components/icons/TwitterIcon.vue';
import type { Route } from '@/router/routes';

export type Link = { is: VNodeTypes; to: Route };

export interface SideBarLink extends Omit<Link, 'is'> {
  isWhenClosed: VNodeTypes;
  isWhenOpen: VNodeTypes;
}

export const sideBarLinks: SideBarLink[] = [
  { to: '/store', isWhenClosed: StoreIcon, isWhenOpen: h('span', 'Store') },
  { to: '/faq', isWhenClosed: FaqIcon, isWhenOpen: h('span', "FAQ's") },
];

export interface SocialLink extends Omit<Link, 'to'> {
  to: string;
}

export const socialLinks: SocialLink[] = [
  { to: 'https://discord.gg/Gzsu4TZtee', is: DiscordIcon },
  { to: 'https://twitter.com/AlchemestryDev', is: TwitterIcon },
  { to: 'https://t.me/aalchemestry', is: TelegramIcon },
];

export const navBarLinks: Link[] = [
  { to: '/store/queue', is: h('span', 'Queue') },
  { to: '/store/random', is: h('span', 'Random') },
];
