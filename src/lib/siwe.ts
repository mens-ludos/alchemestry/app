import {
  createSIWEConfig,
  type SIWECreateMessageArgs,
  type SIWEVerifyMessageArgs,
} from '@web3modal/siwe';
import { SiweMessage } from 'siwe';
import { getAccount } from 'use-wagmi/actions';

import { wagmiConfig } from '@/lib/wagmi';

const domain = window.location.host;
const nonce = Math.floor(Math.random() * 1000000000).toString();

const getNonce = async (address?: string) => {
  //! This should be done through a backend; this is just a demonstration
  //! Nonce should at least 8 symbols long (idk why, but it's a requirement for Siwe)
  return address ? nonce : '';
};

const createMessage = ({ nonce, address, chainId }: SIWECreateMessageArgs) => {
  return new SiweMessage({
    version: '1',
    domain,
    uri: window.location.origin,
    address,
    chainId,
    nonce,
    statement: 'Sign in With Ethereum.',
  }).prepareMessage();
};

const verifyMessage = async ({ message, signature }: SIWEVerifyMessageArgs) => {
  try {
    //! This should be done through a backend; this is just a demonstration.
    const { data, error } = await new SiweMessage(message).verify({ signature, domain, nonce });
    if (error) throw error;
    const { address } = await getAccount(wagmiConfig);
    const { address: recoveredAddress } = data;
    return address?.toLowerCase() === recoveredAddress.toLowerCase();
  } catch (error) {
    return false;
  }
};

const getSession = async () => {
  //! This should be done through a backend; this is just a demonstration.
  const { address, chainId } = await getAccount(wagmiConfig);

  if (!address || !chainId) return null;

  return { address, chainId };
};

const signOut = async () => {
  //! This should be done through a backend; this is just a demonstration.
  return true;
};

export const siweConfig = createSIWEConfig({
  getNonce,
  createMessage,
  verifyMessage,
  getSession,
  signOut,
});
