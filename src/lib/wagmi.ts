import { createConfig, http } from 'use-wagmi';
import { sepolia } from 'use-wagmi/chains';
import { walletConnect, injected, coinbaseWallet, safe } from 'use-wagmi/connectors';

import { env } from '@/env';

export const projectId = env.VITE_PROJECT_ID;

export const metadata = {
  name: 'Alchemestry',
  description: 'Buy tables and get rich',
  url: 'https://alchemestry.com',
  icons: ['https://alchemestry.com/favicon.ico'],
};

export const wagmiConfig = createConfig({
  chains: [sepolia],
  transports: {
    [sepolia.id]: http(),
  },
  connectors: [
    walletConnect({ projectId, metadata, showQrModal: false }),
    injected({ shimDisconnect: true }),
    coinbaseWallet({ appName: metadata.name, appLogoUrl: metadata.icons[0] }),
    safe({ shimDisconnect: true }),
  ],
});
