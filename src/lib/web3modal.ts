import { createWeb3Modal } from '@web3modal/wagmi/vue';

import { siweConfig } from '@/lib/siwe';
import { metadata, projectId, wagmiConfig } from '@/lib/wagmi';

export const initWeb3Modal = () => {
  createWeb3Modal({
    wagmiConfig,
    projectId,
    enableOnramp: true,
    metadata,
    themeMode: 'dark',
    enableAnalytics: true,
    siweConfig,
  });
};
