import { defineStore } from 'pinia';
import { ref } from 'vue';

export const useUiNavbarStore = defineStore('ui/navbar', () => {
  const isOpen = ref(false);

  function toggle() {
    isOpen.value = !isOpen.value;
  }

  return { isOpen, toggle };
});
