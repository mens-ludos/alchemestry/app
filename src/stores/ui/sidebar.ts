import { useMediaQuery } from '@vueuse/core';
import { defineStore } from 'pinia';
import { computed, ref } from 'vue';

export const useUiSidebarStore = defineStore('ui/sidebar', () => {
  const isLg = useMediaQuery('(min-width: 1024px)');
  const _isOpen = ref(true);

  const isOpen = computed(() => (!isLg.value ? false : _isOpen.value));

  function toggle() {
    _isOpen.value = !_isOpen.value;
  }

  return { isLg, isOpen, toggle };
});
