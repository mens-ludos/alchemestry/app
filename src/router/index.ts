import { createRouter, createWebHistory } from 'vue-router';

import { ROUTES } from './routes';

import DefaultLayout from '@/layouts/DefaultLayout.vue';

export const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: ROUTES.ROOT,
      name: 'DefaultLayout',
      component: DefaultLayout,
      redirect: ROUTES.STORE.QUEUE.ROOT,
      children: [
        {
          path: ROUTES.STORE.ROOT,
          name: 'StoreLayout',
          redirect: ROUTES.STORE.QUEUE.ROOT,
          component: () => import('@/layouts/StoreLayout.vue'),
          children: [
            {
              path: ROUTES.STORE.QUEUE.ROOT,
              name: 'StoreQueuePage',
              component: () => import('@/pages/store/StoreQueuePage.vue'),
            },
            {
              path: ROUTES.STORE.RANDOM.ROOT,
              name: 'StoreRandomPage',
              component: () => import('@/pages/store/StoreRandomPage.vue'),
            },
          ],
        },
        { path: ROUTES.FAQ.ROOT, name: 'FaqPage', component: () => import('@/pages/FaqPage.vue') },
      ],
    },
  ],
});
