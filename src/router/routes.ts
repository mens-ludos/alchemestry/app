export const ROUTES = {
  ROOT: '/',
  STORE: {
    ROOT: '/store',
    QUEUE: {
      ROOT: '/store/queue',
    },
    RANDOM: {
      ROOT: '/store/random',
    },
  },
  FAQ: {
    ROOT: '/faq',
  },
} as const;

type FlattenRoutes<T> = T extends object
  ? {
      [K in keyof T]: FlattenRoutes<T[K]>;
    }[keyof T]
  : T;

export type Route = FlattenRoutes<typeof ROUTES>;
